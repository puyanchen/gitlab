# Git 工作流

- 本流程适用于托管在 <https://gitlab.com> 生产 git 库上的项目
- 开放库 <https://gitlab.com> 上的项目可以参考[gitlab flow](https://gitlab.com/help/workflow/gitlab_flow.md)或者[github flow](https://guides.github.com/introduction/flow/)

## 一般工作流程

总体原则: 只有1个或2个人（一般是项目负责人）对master分支有写权限。其他人只能提交merge request, 由这1，2个人将代码合并到master分支中。


![git-flow](git-flow.png)


+ 在第2步创建任务分支时，以master分支作为基础，创建新的任务分支。任务分支的命名应为 用户名-JIRA号 例如:

  ```
  mjy-PREPROCESS-15
  ```

  > 有时为了直观，也可以在JIRA号后面增加自定义信息，但自定义信息不宜过长。


+ 在第6步发起Merge Request之后，如需要做其他任务，应当先checkout 到master分支，再创建新分支做其他任务。这样可以2个任务并行开发并不相互影响。


+ 如果code review后需要修改（包括现有master代码发生冲突），需checkout到merge request对应的任务分支，对代码进行修改，然后再次push. 此时无需重新发起merge request, 改动会自动更新到merge request中，并自动邮件通知code reviewer.


+ 第11步删除分支的时候，只删除已经被合并到master的分支。


## 多版本并行开发流程


+ 如果某一时段需要多版本并行开发，则需要由项目负责人先从master分支创建出版本分支，例如v1.0.0和v1.1.0.


+ 针对v1.0.0和v1.1.0分支，应用上一章节的一般工作流程。当v1.0.0或v1.1.0分支开发完成后，则将分支代码合并到master. 最后回归到上一章节的一般工作流程。


+ 如果2个版本互不干涉，以后不会合并到master, 则需要专门为其中一个版本建立新的仓库，而不能在一个仓库中开发; 另一个版本直接在master上使用一般工作流程开发。


## 版本发布

选择最新commit, 添加一个tag即可。
