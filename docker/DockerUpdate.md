# 部署流水线上线手册（参考模板）
## 1.检查

由于之前的`Docker`采用的是二进制安装方式，本次升级实质是`移除旧版二进制安装，安装新版本`

因此在升级之前要先停止运行的容器,删除相关的配置

> 注: 保留`Docker`存放容器和镜像的目录`/var/lib/docker`或者自定义挂载的目录,在升级之前可以选择备份

### 1.1移除相关进程

- 关闭还在运行的容器
- 通过`ps -ef|grep docker`查找`Docker`相关进程进行`kill`

### 1.2移除相关配置

- 删除`/usr/local/bin`下`Docker`相关的内容
- 删除`/var/run/`下`Docker`有关的如: `/var/run/docker`,`/var/run/docker.sock`,`/var/run/docker.pid`,注：如果在该环境下有部署类似于`rancher,gitlab-runner`这些对`docker.sock`有改动的容器，可保留
- 删除`/etc/docker` 中的配置
- 删除`/root/.docker`中的配置

## 2.操作步骤

### 2.1依赖安装

```bash
apt-get install libltdl7
```

### 2.2安装脚本

```bash
#安装 docker
sudo su -
wget ftp://ftp.cqrd.com/software/docker/x86_64/docker-ce_17.12.0_ce-0_ubuntu_amd64.deb
dpkg -i docker-ce_17.12.0_ce-0_ubuntu_amd64.deb
mkdir -p /etc/systemd/system/docker.service.d

#网络代理配置
cat << EOF > /etc/systemd/system/docker.service.d/proxy.conf
[Service]
Environment="HTTP_PROXY=http://172.17.18.80:8080"
Environment="HTTPS_PROXY=http://172.17.18.80:8080"
Environment="NO_PROXY=.x,hub.com,h.com,.cae.com"
EOF

#启动参数配置（包括 docker 镜像代理配置）
cat << EOF > /etc/docker/daemon.json
{
    "live-restore": true,
    "bip": "172.172.172.1/24",
    "dns": ["172.26.61.250"],
    "registry-mirrors": ["https://mirror.cae.com"],
    "insecure-registries": ["hub.x", "hub.com", "h.com"]
}
EOF

exit
#将当前用户加入到 docker 组，以便可以使用当前用户执行 docker 指令
# sudo addgroup docker
# sudo usermod -aG docker $USER 

#刷新配置并重启 docker
sudo systemctl daemon-reload
sudo systemctl restart docker

```

> 注：升级完后一定要重启确认,通过`ps -ef|gerp docker`查看当前进程内的`docker`是否是`/usr/bin/dockerd -H fd://` 而不是通过启动脚本启动的

## 3.测试验收

- 正常登陆`hub.x,h.com`
- 拖拉镜像正常,上传镜像正常
- 运行容器启动进行正常
- 之前存在的镜像已经存在
- 之前存在的容器重启成功

## 4.回滚操作

### 4.1移除新安装的Docker

通过命令移除

`apt-get remove docker-ce`

### 4.2删除相关配置

```bash
rm -rf /etc/systemd/system/docker.service.d/

rm -rf /etc/docker/

rm -rf /root/.docker/
```

### 4.3采取之前的安装步骤进行安装

```bash
# 下载
sudo curl ftp://ftp.cqrd.com/software/docker/x86_64/docker-17.03.1-ce.tgz

# 解压
sudo tar --strip-components=1 -xvzf docker-17.03.1-ce.tgz -C /usr/local/bin
```

### 4.4通过启动脚本启动

```bash
all_proxy=http://172.17.18.84:8080 \
no_proxy=hub.com,h.com,.cae.com,hub.x \
/usr/local/bin/dockerd \
--live-restore \
--registry-mirror=https://mirror.cae.com  \
--bip=172.172.172.1/24 \
--dns=172.26.61.250  \
--insecure-registry=hub.com \
--insecure-registry=hub.x \
--insecure-registry=h.com \
# 注：是否将docker的镜像和容器等存放地址挂载出去，默认地址为 /var/lib/docker
-g /opt/lib/docker &\
```