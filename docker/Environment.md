# Environment环境代理配置

```bash
sudo su
echo "nameserver 172.26.61.250" > /etc/resolvconf/resolv.conf.d/base
echo "nameserver 172.26.61.249" >> /etc/resolvconf/resolv.conf.d/base
echo "nameserver 172.26.61.250" > /etc/resolv.conf
echo "nameserver 172.26.61.249" >> /etc/resolv.conf

#当前用户代理
cat << EOF >> ~/.bashrc 
export ftp_proxy=http://172.17.18.80:8080
export http_proxy=http://172.17.18.80:8080
export https_proxy=http://172.17.18.80:8080
export all_proxy=http://172.17.18.80:8080
export no_proxy=localhost,127.0.0.1,*.x,.cqrd.com,.cqdc.com,.coe.com,.cae.com,.paas.com,.travelsky.com,.travelsky.net
EOF

#root用户代理
sudo su -
cat << EOF >> ~/.bashrc
export ftp_proxy=http://172.17.18.80:8080
export http_proxy=http://172.17.18.80:8080
export https_proxy=http://172.17.18.80:8080
export all_proxy=http://172.17.18.80:8080
export no_proxy=localhost,127.0.0.1,*.x,.cqrd.com,.cqdc.com,.coe.com,.cae.com,.paas.com,.travelsky.com,.travelsky.net
EOF

#apt代理
cat << EOF > /etc/apt/apt.conf
Acquire::http::Proxy "http://172.17.18.80:8080";
Acquire::http::Proxy::172.26.1.4 "DIRECT";
Acquire::http::Proxy::ftp.cqrd.com "DIRECT";
EOF
```