## Docker旧版安装

> V17.03以及之前的版本采用二进制安装,安装方式如下

## 安装根证书

```bash
sudo su -
curl http://ca.cae.com/ca.cae.com.crt -o /usr/local/share/ca-certificates/ca.cae.com.crt
update-ca-certificates
```

## 内网下载安装包
```bash
# 下载
sudo curl ftp://ftp.cqrd.com/software/docker/x86_64/docker-17.03.1-ce.tgz

# 解压
sudo tar --strip-components=1 -xvzf docker-17.03.1-ce.tgz -C /usr/local/bin
```

## Docker启动脚本启动
```bash
all_proxy=http://172.17.18.84:8080 \
no_proxy=hub.com,h.com,.cae.com,hub.x \
/usr/local/bin/dockerd \
--live-restore \
--registry-mirror=https://mirror.cae.com  \
--bip=172.172.172.1/24 \
--dns=172.26.61.250  \
--insecure-registry=hub.com \
--insecure-registry=hub.x \
--insecure-registry=h.com \
# 注：是否将docker的镜像和容器等存放地址挂载出去，默认地址为 /var/lib/docker
-g /opt/lib/docker &\
```

## 验证
- 正常登陆`hub.x,h.com`
- 拖拉镜像正常,上传镜像正常
- 运行容器启动进行正常