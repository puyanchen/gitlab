Gitlab Issue & MR 模板的使用
-------------------

## 概述

通过使用模板，告知用户在创建Issue或Merger Request时的规则，按照模板指定的格式进行填写。

每个GitLab项目都可以定义自己的描述模板，通常模板位于项目的根目录。

模板必须用Markdown编写，且必须保存默认分支上，才能生效。

## 模板的创建

1. 首先在项目根目录创建`.gitlab`文件夹。
2. 不同功能的模板存放在不同的文件夹中：

|功能点   | 目录 |
|:---------|:------|
| issue： |`.gitlab/issue_templates/` |
|    merge request : | `.gitlab/merge_request_templates/` |
3. 创建模板文件(`.md`)文件，这里以`issue`为示例，创建`issue.md`
```md
(标题)
-------------------
- AS  :
- IWT :
- ST  :
- 验收标准：
    - (若干条件)
```
此处可根据项目需要自行调整。

4. 提交模板到gitlab默认分支。

## 模板的使用
创建 issue 或 mr 时，选择需要使用的模板：

![image](./select_template.png)

可以根据不同的需求制定多个模板。

更多详情,请参考[官方文档](https://docs.gitlab.com/ce/user/project/description_templates.html)。
