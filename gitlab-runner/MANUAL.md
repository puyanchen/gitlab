# 为项目配置gitlab-runner
为了执行持续集成，每个项目默认都分配了`gitlab-runner`.

## 查看gitlab-runner状态
- 点击项目右上角设置图标，选择`runners`:
> ![runner-menu](images/runner-menu.png)

- 查看runner是否处于激活状态:
> ![runner-status](images/runner-status.png)

# 使用gitlab-runner执行持续集成任务
参考首页的[使用手册](https://gitlab.cae.com/cqrd/gitlab/tree/master#%E4%BD%BF%E7%94%A8%E6%89%8B%E5%86%8C) , 里面有各种常见任务的设置示例。
