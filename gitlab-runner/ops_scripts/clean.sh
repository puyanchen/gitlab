#!/bin/bash
# execute: nohup sh clear.sh >> log.txt &

MM_TEAM=1jytzxd5ubyk9ja1q1fxb9srhh
MM_CHANNEL=eyrrbo3mitnjjnak347jsa1byy
ThresholdValue=85
DISK=/dev/mapper/drone--vg-root
gap=60

sendMessageByMM()
{
    DISK_STATE=`df -h | grep ${DISK}`
    DATE=`date`
    MESSAGE="TASK: Gitlab-Runner 自动清理\\n\\nDATE: ${DATE}\\nMES  : 自动清理任务执行失败\\nDISK_INFO: ${DISK_STATE}"
    TOKEN=`curl -isk -d "{\"login_id\":\"notify\",\"password\":\"notify\"}" http://mm.paas.x/api/v3/users/login |grep Token:|cut -d' ' -f2|sed s/[[:space:]]//g`
    curl -sk -H "Authorization:Bearer $TOKEN" -d "{\"channel_id\":\"$MM_CHANNEL\",\"message\":\"$MESSAGE\"}" http://mm.paas.x/api/v3/teams/$MM_TEAM/channels/$MM_CHANNEL/posts/create
}

while true
do
    #获取磁盘容量
    var=`df -h | grep ${DISK} | awk {'print $5'} | cut -c -2`
    echo "`date`   Used capacity: ${var}."
    if [ "${var}" -gt "${ThresholdValue}" ]
    then
        echo "Perform cleanup functions."
        /usr/local/bin/docker system prune -af

        #如清理后还是磁盘不足，则停止任务，通知管理员。
        var=`df -h | grep ${DISK} | awk {'print $5'} | cut -c -2`
        if [ ${var} -gt ${ThresholdValue} ]
        then
            echo "Clean fail."
            sendMessageByMM 
            break
        fi
    fi
    sleep ${gap}
done
