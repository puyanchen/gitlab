mkdir -p /opt/sh
cd /opt/sh

echo "install certificates"
curl http://ca.cae.com/ca.cae.com.crt -o /usr/local/share/ca-certificates/ca.cae.com.crt
update-ca-certificates
mkdir -p /etc/docker/certs.d/mirror.cae.com
cp /usr/local/share/ca-certificates/ca.cae.com.crt /etc/docker/certs.d/mirror.cae.com/ca.crt
mkdir -p /etc/docker/certs.d/registry.cae.com
cp /usr/local/share/ca-certificates/ca.cae.com.crt /etc/docker/certs.d/registry.cae.com/ca.crt

echo "install docker"
wget ftp://ftp.cqrd.com/software/docker/x86_64/docker-17.03.1-ce.tgz
tar xzf docker-17.03.1-ce.tgz 
mv docker/* /usr/local/bin

echo "start docker"
mkdir -p /opt/lib/docker
mkdir -p /opt/log
cat << EOF > docker.sh
all_proxy=http://172.17.18.80:8080 \
no_proxy=hub.com,h.com,.cae.com \
dockerd \
--live-restore \
--registry-mirror=https://mirror.cae.com  \
--bip=172.172.172.1/24 \
--dns=172.26.61.250  \
--insecure-registry=hub.com \
--insecure-registry=h.com \
-g /opt/lib/docker \
>/opt/log/docker 2>&1 &
EOF
sh docker.sh

echo "set docker auto start"
mv /etc/rc.local /etc/rc.local.`date +%Y-%m-%d-%s`
echo "#!/bin/sh -e" > /etc/rc.local
echo "sh /opt/sh/docker.sh" >> /etc/rc.local
echo "exit 0" >> /etc/rc.local
chmod a+x /etc/rc.local

echo "write runner config"
mkdir -p /opt/volumes/gitlab-runner/etc/gitlab-runner/certs
cd /opt/volumes/gitlab-runner/etc/gitlab-runner/certs
cp /usr/local/share/ca-certificates/ca.cae.com.crt git.cae.com.crt
cp git.cae.com.crt reg.cae.com.crt
cd ..
cat << EOF > config.toml
concurrent = 20
check_interval = 0
[[runners]]
  name = "run"
  url = "https://git.cae.com"
  token = "xxx"
  executor = "docker"
  [runners.docker]
    tls_verify = false
    image = "alpine:3.4"
    privileged = true 
    disable_cache = false
    volumes = ["/cache"]
    pull_policy = "if-not-present"
  [runners.cache]
EOF

