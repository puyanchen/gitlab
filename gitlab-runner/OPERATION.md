# 部署gitlab-runner

生产环境的gitlab-runner是以docker容器的形式部署的。  

这里介绍如何使用docker部署和启动gitlab-runner.

> 参考资料：[官方文档](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/docker.md)

## 步骤
- 安装docker:
```
curl -sSL https://get.docker.com/ | sh
```

- 配置docker:
```
docker run -d --name gitlab-runner --restart always \
  -v /etc/gitlab-runner:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
```

- 注册runner:

首先需要获取一个runner token.   

> 参考：http://docs.gitlab.com/ce/ci/runners/README.html#registering-a-shared-runner

获取token后，执行以下命令:
```
docker exec -it ${gitlab-runner-name} gitlab-runner register
```
在接下来的提示中输入相关信息:

```
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com )
在这里输入你的域名

Please enter the gitlab-ci token for this runner
在这里输入刚才获取的token

Please enter the gitlab-ci description for this runner
在这里输入runner名字
INFO[0034] fcf5c619 Registering runner... succeeded

Whether to run untagged jobs [true/false]:
[false]: true

Whether to lock Runner to current project [true/false]:
[true]: false

Please enter the executor: shell, docker, docker-ssh, ssh?
docker

Please enter the Docker image (eg. ruby:2.1):
ruby:2.1(或者alpine:3.4)

INFO[0037] Runner registered successfully. Feel free to start it, but if it's
running already the config should be automatically reloaded!
```

## gitlab-runner配置

`gitlab-runner`的配置可参如下：

```toml
# 最大连接数
concurrent = 10 
check_interval = 0
[[runners]]
  name = "gitlab-runner-10.4.0"
  url = "https://gitlab.cae.com/"
  token = ""
  executor = "docker"
  [runners.docker]
    tls_verify = false
    image = "alpine:3.4"
    privileged = true # 开启特权模式
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
    pull_policy = "if-not-present"
  [runners.cache]
```

## 验证
执行以下命令查看是否有新的`gitlab-runner`进程

```
docker ps
```

# 清理空间

- 当`gitlab runner` 所在服务器空间即将占满时，可以使用以下指令释放空间

```
docker system prune
```

- 进行升级操作后利用自动清理脚本清理缓存

```bash
#!/bin/bash
ThresholdValue=85
DISK=/dev/mapper/drone--vg-root
gap=60
while true
do
    #获取磁盘容量
    var=`df -h | grep ${DISK} | awk {'print $5'} | cut -c -2`
    echo "`date`   Used capacity: ${var}."
    if [ "${var}" -gt "${ThresholdValue}" ]
    then
        echo "Perform cleanup functions."
        docker system prune -af

        #如清理后还是磁盘不足，则停止任务，通知管理员。
        var=`df -h | grep ${DISK} | awk {'print $5'} | cut -c -2`
        if [ ${var} -gt ${ThresholdValue} ]
        then
            echo "Clean fail."
            break
        fi
    fi
    sleep ${gap}
done
```

# 修改open files最大连接数

粘贴以下内容到`/etc/security/limits.conf`

```
*         hard    nofile      500000
*         soft    nofile      500000
root      hard    nofile      500000
root      soft    nofile      500000
```
