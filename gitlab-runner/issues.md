# gitlab-runner问题收集


`gitlab-runner`相关问题可在`issues`页面查看  

https://gitlab.cae.com/cqrd/gitlab/issues


## 常见问题

- [#57 开放库&生产库gitlab-runner升级至V10.4.0](https://gitlab.cae.com/cqrd/gitlab/issues/57)

- [#56 git-runner 运行失败
](https://gitlab.cae.com/cqrd/gitlab/issues/56)
```xml
ERROR: Job failed (system failure): Error response from daemon: connection error: desc = "transport: dial unix /var/run/docker/containerd/docker-containerd.sock: connect: connection refused"
```

- [#59 ERROR: Preparation failed - BUG
](https://gitlab.cae.com/cqrd/gitlab/issues/59)

```xml
ERROR: Preparation failed:Cannot connect to the Docker daemon at unix://ar/run/docker.sock. Is the docker daemon running?
```

- [#71 Job被挂起，请帮忙排查一下原因和给予解决方案，谢谢！
](https://gitlab.cae.com/cqrd/gitlab/issues/71)

- [#35 gitlab runner所在机器docker无响应
](https://gitlab.cae.com/cqrd/gitlab/issues/35)