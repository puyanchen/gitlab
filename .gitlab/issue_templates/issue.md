- 作为一个 <角色>
- 我想要 <需求>
- 以便于 <目的>

---
## 验收标准
- <标准1>
- <标准2>
- <标准3>

---
## 上下文
- pipeline repository: <比如> https://gitlab.cae.com/cqrd/gitlab
- pipeline job: <比如> https://gitlab.cae.com/cqrd/gitlab/-/jobs/16748