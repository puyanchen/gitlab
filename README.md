# 使用手册
- [Git 工作流](flow)
- [Issue 使用](template)
- [Gitlab CI 是如何运作的](https://gitlab.com/cqrd/gitlab/tree/run)
- [如何使用 SSH 连接远程服务器](https://gitlab.com/cqrd/gitlab/tree/ssh)
- [artifacts 的使用](https://gitlab.com/cqrd/gitlab/tree/artifacts)
- [在多个 job 之间使用 cache](https://gitlab.com/cqrd/gitlab/tree/job-cache)
- [在多次集成之间使用 cache](https://gitlab.com/cqrd/gitlab/tree/build-cache)
- [gitlab registry 的使用](https://gitlab.com/cqrd/gitlab/tree/registry)
- [如何使用 Gitlab CI 构建 docker 镜像](https://gitlab.com/cqrd/gitlab/tree/gitlab-ci-image)
- [迁移svn项目到gitlab](gitlab/MANUAL.md#%E4%BB%8Esvn%E8%BF%81%E7%A7%BB%E9%A1%B9%E7%9B%AE%E5%88%B0gitlab)


# 部署
> 如果有兴趣，你可以根据以下文档自己部署整个持续集成环境

- [docker](docker)
- [gitlab](gitlab)
- [gitlab-runner](gitlab-runner)
- [jenkins](jenkins)
