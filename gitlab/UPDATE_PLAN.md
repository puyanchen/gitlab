部署流水线上线手册（参考模板）
---------------------------------------
## 1.检查
- 磁盘空间检测

因有备份需求，需提前检查部署服务器的空间是否足够。
```sh
df -h|XXXXXXXX 
```
要求：保证剩余空间导到已使用空间的1.5倍。

- 镜像检测

升级镜像准备，防止因为网络原因，无法下载。
```sh
#下载镜像
docker pull gitlab/gitlab-ce:${version}
#查看已下载镜像
docker images | grep gitlab/gitlab-ce
```

## 2.操作步骤
2.1 清除备份

删除上上一版本数据备份
```sh
rm -f /backup/gitlab-{pre-pre-version}-data-{date}.tar
```
2.2 停止容器
```sh
docker update --restart=no gitlab-{pre-version}
docker exec gitlab gitlab-ctl stop unicorn
docker exec gitlab gitlab-ctl stop sidekiq
docker exec gitlab gitlab-ctl stop nginx
docker stop gitlab gitlab-{pre-version}
```
2.3 备份数据卷
```sh
sudo su
mkdir -p /opt/back/gitlab-XX-${DATE}
cp -rf /opt/volumes/gitlab /opt/back/gitlab-XX-${DATE}
```
2.4 启动新容器
```
docker run -d --hostname gitlab \
    --publish 443:443 --publish 80:80 --publish 22:22 \
    --name gitlab \
    --restart always \
    --volume /opt/volumes/gitlab/etc/gitlab:/etc/gitlab \
    --volume /opt/volumes/gitlab/var/log/gitlab:/var/log/gitlab \
    --volume /opt/volumes/gitlab/var/opt/gitlab:/var/opt/gitlab \
    gitlab/gitlab-ce:{version}
```
2.5 执行测试用例
```sh
#检查容器状态
docker exec gitlab-{version} gitlab-rake gitlab:check SANITIZE=true
```
2.6 收尾工作
```sh
#删除不需要的文件
docker rmi gitlab/gitlab-ce:{version}
#归档压缩本次备份（是否需要执行）
nohup tar zcf /backup/gitlab-{pre-version}-data-{date}.tar /opt/volumes/gitlab &
```

## 3.回滚步骤
- 使用上个版本的容器进行回滚
```sh
docker start gitlab-{pre-version}
docker update --restart=yes gitlab-{pre-version}
```

- 使用外挂数据卷启动新容器
```sh
docker run -d --hostname gitlab \
    --publish 443:443 --publish 80:80 --publish 22:22 \
    --name gitlab \
    --restart always \
    --volume /opt/volumes/gitlab/etc/gitlab:/etc/gitlab \
    --volume /opt/volumes/gitlab/var/log/gitlab:/var/log/gitlab \
    --volume /opt/volumes/gitlab/var/opt/gitlab:/var/opt/gitlab \
    gitlab/gitlab-ce:{old_version}
```
