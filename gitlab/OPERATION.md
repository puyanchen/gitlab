# 部署gitlab

## 准备
1. 申请COE账号
2. 配置dns服务器: 172.26.61.250
3. 确保能访问www.coe.com

## 部署步骤
- 访问www.coe.com并登录

- 点击右上角profile图标，选择 "我的项目"

- 在右侧输入项目名称和命名空间

> ![create](images/create.png)

- 上传官方镜像

> 需要先在[本地安装 docker](https://gitlab.cae.com/cqrd/registry)

```sh
docker pull gitlab/gitlab-ce:8.15.4-ce.1
docker tag gitlab/gitlab-ce:8.15.4-ce.1 h.com/gitlab/gitlab-ce:8.15.4-ce.1
docker login h.com
docker push h.com/gitlab/gitlab-ce:8.15.4-ce.1
```

注: `h.com`的账户密码与`www.coe.com`相同

- 在项目列表中选择刚才创建好的项目

> ![select-project](images/select-project.png)


- 点击 "添加项目镜像到DAG"

> ![add-dag](images/add-dag.png)


- 选择 gitlab/gitlab-ce

> ![select-mirror](images/select-mirror.png)


- 选择最新tag, 并且删除最后一个卷挂载点

> ![setup-mirror](images/setup-mirror.png)


- 按照下图设置内存，cpu核数, cpu占用率, 工号，邮箱，镜像别名, 描述信息

> ![setup-mirror-final](images/setup-mirror-final.png)


- 添加完成后可以看到贴纸

> ![block](images/block.png)


- 点击贴纸，再点击"创建胶囊云主机"即完成gitlab部署。

> ![start](images/start.png)


## 验证
- 点击贴纸下方的编号，确认gitlab网站能够访问。
> ![validate](images/create-validate.png)

- 注册一个测试帐号，创建一个项目，验证是否能成功。
- 将新建的项目clone到本地，修改后将改动push到服务器，验证是否能成功。

# 备份

## 步骤
gitlab生产环境目前部署在RAID5磁盘阵列上，未使用特别的备份操作。

如果gitlab部署在普通硬盘上，可采用外接新存储设备，使用gitlab的官方备份命令进行备份，参考以下脚本:

```
[root@gitlab ~]# vi gitlab-backup.sh
#/bin/bash
# 调用gitlib的备份命令备份，备份出来的文件存储在 /var/opt/gitlab/backups 中。注意，/var/opt/gitlab/backups 应为一个外接的存储设备。
gitlab-rake gitlab:backup:create
# 在本服务器上删除15天前创建的备份文件
find /var/opt/gitlab/backups/ -maxdepth 1 -name '*.tar' -ctime +15 -delete
```

然后在crontab中设置定时执行备份:

```
[root@gitlab ~]# crontab -e
no crontab for root - using an empty one

0 4 * * * source /root/gitlab-backup.sh >/dev/null 2>&1 &
```

## 验证
如果使用 `gitlab-rake gitlab:backup:create` 进行备份，可访问备份目录 `var/opt/gitlab/backups` 检查备份文件是否存在。

# 恢复

## 步骤
gitlab生产环境目前部署在RAID5磁盘阵列上，不在讨论范围内。如果使用了`gitlab-rake gitlab:backup:create` 进行备份，可使用以下命令进行恢复:

```
# 首先停止gitlab服务
sudo service gitlab stop

# BACKUP后面的日期与备份文件的文件名中日期对应
gitlab-rake gitlab:backup:restore BACKUP=1393513186_2014_02_27

# 启动并且检查gitlab
gitlab-ctl start
gitlab-rake gitlab:check SANITIZE=true
```

## 验证
访问gitlab网页，检查数据是否恢复到指定日期。

# 升级

## 准备
升级gitlab以前，需要确认:
- 当前gitlab版本
- 从当前gitlab版本是否能直接升级到目标版本。参考官方文档 https://about.gitlab.com/update/
- 确认升级影响的用户和系统，提前5个工作日通知用户及系统负责人
- 升级需要在非工作时间进行
- 升级步骤需要先在本地演练

## 升级步骤
### 升级准备

注意: 步骤1为演练所需步骤, 在gitlab.cae.com和git.cae.com上升级不需要再搭建gitlab环境，从步骤2开始。

1. 在Ubuntu 16.04.1上搭建gitlab环境，版本请与当前生产环境一致。例如如果生产环境使用版本是9.1.3，则使用镜像 gitlab/gitlab-ce:9.1.3-ce.0 搭建。

```
docker pull gitlab/gitlab-ce:9.1.3-ce.0
docker run -d --hostname gitlab \
              --publish 443:443 --publish 80:80 \
              --name gitlab \
              --restart always \
              --volume /srv/gitlab/config:/etc/gitlab \
              --volume /srv/gitlab/logs:/var/log/gitlab \
              --volume /srv/gitlab/data:/var/opt/gitlab \
              gitlab/gitlab-ce:9.1.3-ce.0
```
2.访问gitlab服务，新建测试项目，项目中需要建立Issue，Merge request，Branch，Code。
3.准备升级所需镜像。 如果是跨版本升级，请准备所有中间版本以及目标版本的镜像。以下命令中的${VERSION}为版本镜像。

```
docker pull gitlab/gitlab-ce:${VERSION}
```

gitlab镜像列表：https://hub.docker.com/r/gitlab/gitlab-ce/tags/

### 数据备份
进入到gitlab的docker服务中，运行gitlab-rake gitlab:backup:create命令完成备份。

```
docker exec -ti gitlab /bin/bash
gitlab-rake gitlab:backup:create
```

备份文件保存目录：/var/opt/gitlab/backups

升级GitLab

1)停止正在运行的gitlab服务

```
docker stop gitlab
```

2)删除搭建GitLab时所使用的container。

```
docker rm gitlab
```

3)搭建下一个版本gitlab服务。

重复步骤1至3，直到升级到目标版本。

### 验证
- 前往 https://{gitlabhost}/help 查看版本信息，确认版本号已更新。
- 进入gitlab 查看测试项目的Issue，Merge request，Branch，Code是否完整。

### 回滚

如升级出现异常，则使用之前备份的数据进行回滚。

1.停止当前正在运行的docker gitlab服务，并删除container。

```
docker stop gitlab
docker rm gitlab
```

2.然后用备份时的版本镜像，构建新的gitlab服务。参考准备升级第1步。

3.进入gitlab服务，停止nginx, sidekiq, unicorn服务。

```
docker exec -ti gitlab /bin/bash
gitlab-ctl stop unicorn
gitlab-ctl stop sidekiq
gitlab-ctl stop nginx
```

4.执行以下命令对数据进行恢复:

```
# BACKUP后面的日期与备份文件的文件名中日期对应，如文件名为：
     1497928375_2017_06_20_gitlab_backup.tar

gitlab-rake gitlab:backup:restore BACKUP=1497928375_2017_06_20
```

5.启动gitlab所有服务。并执行以下命令对gitlab系统状态进行检查:

```
gitlab-ctl start
gitlab-rake gitlab:check SANITIZE=true
```

如有异常，根据提示信息进行修复。

6.对已有项目，merge request, issue, pipeline, code做检查。

# 迁移
利用备份和恢复功能进行迁移，官方文档 https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/raketasks/backup_restore.md

## 步骤
- 备份源服务器gitlab数据，参考`备份`章节
- 在目标服务器安装与源服务器相同版本的gitlab
- 将源服务器的备份文件复制到目标服务器相同的目录下
- 在目标服务器恢复gitlab数据，参考`恢复`章节

## 验证
访问部署在目标服务器上的gitlab, 确认repository, 用户, 分支，commit历史都保存完好。

# 下线
生产系统如需下线，请先联系wbdeng@travelsky.com , 并抄送majingyu@travelsky.com

自行搭建的gitlab系统如需下线，请先确认是否有其他用户或系统在使用，如无用户或系统使用，可立即下线；如有其他用户或系统在使用，应提前至少5个工作日通知正在使用的用户和系统，确认所有用户和系统都停止使用您的gitlab系统后，再下线。
