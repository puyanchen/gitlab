# 配置管理

## 平台管理员

作为gitlab的平台管理员，可以设置gitlab的各种参数。在此文档中仅列出常用的操作和设置，完整的平台功能参见[官方文档](https://docs.gitlab.com/ce/README.html)。

### 重置用户密码

通常情况下，用户忘记密码可以通过`找回密码`功能自行重置密码。为了应对突发情况（例如邮件服务器突然无法访问等），也可以由管理员帮忙重置密码。

1. 单击`Users` tab项，进入Users管理页面
![admin-users-panel](images/admin-users-panel.png)

2. 在搜索框中，搜索需要重置密码的用户名
![admin-search-users](images/admin-search-user.png)

3. 点击搜索到的账号，进入配置页面
![admin-search-user-result](images/admin-search-user-result.png)

4. 在用户配置页面点击右上角`Edit`按钮
![admin-edit-user](images/admin-edit-user.png)

5. 输入用户密码
![admin-reset-user-pwd](images/admin-reset-user-pwd.png)

### 新建用户

1. 单击`Users` tab项, 进入Users管理页面
![admin-users-panel](images/admin-users-panel.png)

2. 点击`New User`按钮

  ![admin-users-panel](images/admin-new-user.png)

3. 输入新用户信息。注意，用户的初始密码会发到用户邮箱，用户第一次登陆时会被要求重置密码。
![admin-users-panel](images/admin-new-user-edit.png)

## 组Owner
在GitLab中的项目可以通过两种不同的方式进行组织：

- 在自己的单个项目的命名空间中，例如您的名称/项目-1。
- 在某个组下。

如果是在一个组下组织项目，它的工作方式就像一个文件夹，可以管理组成员的权限和对项目的访问。

### 创建组

1. 点击左上角的三个栏杆展开左侧栏，然后导航到**Groups**。
![左侧栏](./images/create_new_group_sidebar.png)

2. 进入您的群组信息面板后，点击**New group**。
![群组信息](./images/create_new_group_info.png)

3. 填写所需信息：
	1. 设置**Group path**，它将是您的项目将被托管的命名空间（路径只能包含字母，数字，下划线，破折号和点;它不能以破折号开头或以点结尾）。
    2. （可选）您可以添加描述，以便其他人可以简要了解此组是什么。
    3. （可选）为您的项目选择和头像。
    4. 选择可见性级别。

4. 最后，单击**Create group**按钮。

### 创建项目
向组中添加新项目有两种不同的方法：

- 选择一个组，然后单击**New Project**按钮。
![新建项目按钮](./images/create_new_project_from_group.png)

- 在创建项目时，从下拉菜单中选择已创建的组命名空间。
![选择已创建的组命名空间](./images/select_group_dropdown.png)

然后你可以看到几个选项：
![](./images/create_new_project_info.png)

填写所需信息：

1. **Project name**是您的项目的名称（您不能使用特殊字符，但您可以使用空格，连字符，下划线甚至emojis）。
2. **Project description**是可选的，将显示在项目的仪表盘中，以便其他人可以简要了解您的项目是什么。
3. 选择一个可见性级别。
4. 您还可以导入现有项目。

最后，单击**Create project**按钮。

### 添加成员
在群组的面板中，选择**Members**。
![Members](./images/create_members_from_group.png)

填写所需信息：

1. 选择成员（通过名称，用户名或电子邮件搜索用户，或使用其电子邮件地址邀请新用户）。
![选择成员](./images/select_user_to_group.png)

2. 选择角色。
![选择角色](./images/select_role_to_user.png)

3. 选择失效日期（可选，不选择表示永久；在此日期，用户将自动失去对该组及其所有项目的访问权限）。
![](./images/select_user_expiration_on_group.png)

最后，单击**Add to group**按钮。

### 从svn迁移项目到gitlab
如需将svn上的项目转移到gitlab, 并保留历史记录以及分支信息，请按照以下步骤操作。

#### 准备工具
git、git-svn、ruby、svn2git、Linux环境(推荐Ubuntu 16.04 TLS)  

#### 操作步骤
- 安装对应的环境
```
sudo apt-get git git-svn ruby
gem install svn2git   
```
- 创建一个目录，即为git本地仓库，如GDS-TWT
- 进入GDS-TWT
- 在GDS-TWT目录下创建userinfo.txt文件，存放svn账号→git账号映射，如：
```
zhangsan=zhangsan<zhangsan@travelsky.com>
lisi=lisi<lisi@travelsky.com>
```
- 组装svn2git初始化并拉取代码到git本地仓库的命令
```
svn2git http://172.17.18.80/svn/twterminator  --authors ./userinfo.txt --username chengkx  --exclude twt-docs --exclude "TWT-上线*" --exclude "TWT-测试*" --metadata --verbose
```
**可根据实际需要组装执行的命令，文档参考地址：** https://github.com/nirvdrum/svn2git  
**备注1**：如果svn分支不是按照trunk/branches/tags这种模式配置的，则需要显示的通过参数分别指定路径。  
**备注2**：如果过程当中提示一些账号没有，则添加上后，继续执行命令，组装好的svn2git命令，则会续传。如果执行的次数较多，会出现找不到对应的链接的情况，则只需要将.git/config清空，再次执行组装好的svn2git命令即可。
- 命令执行完以后，SVN库上的内容以及历史记录，被拉取到本地，可通过一些git指令查看:

 `git branch -a `    查看所有分支  
 只要git环境配置正确，接下来可以直接将本地库中的代码push到git.cae.com上面  
 `git push -u origin master`  推送master(trunk)分支到Git服务器  
 `git push -u origin TWT1.14` 推送branch到Git服务器  
 `git push -u origin releases`  推送tags下的releases到Git服务器
   
>备注：因为git.cae.com这个地址是https的，如果遇到SSL问题的时候(`git SSL certificate problem: unable to get local issuer certificate`), 需要执行命令：

>```sh
git config --global http.sslVerify false`
```

## 项目Owner

### 创建项目

在您的面板中，您可以使用**New Project**绿色按钮创建一个新项目。
接下来的操作与**组Owner**中的[创建项目](#创建项目)基本一致，具体可以参考[创建项目](#创建项目)。

### 添加成员
为项目添加成员需要如下几步：

在项目面板中，点击右上角齿轮形状按钮，在下拉菜单中选择**Members**。
填写所需信息：

1. 选择成员（通过名称，用户名或电子邮件搜索用户，或使用其电子邮件地址邀请新用户）。
![选择成员](./images/select_user_to_group.png)

2. 选择角色。
![选择角色](./images/select_role_to_user.png)

3. 选择失效日期（可选，不选择表示永久；在此日期，用户将自动失去对该组及其所有项目的访问权限）。
![](./images/select_user_expiration_on_group.png)

最后，单击**Add to project**按钮。

### 分支保护
GitLab的权限根据对存储库和分支机构的读取或写入权限的想法进行了根本的定义。为了防止人们混淆历史或推送代码，我们创建了受保护的分支机构。
默认情况下，受保护的分支有四个简单的情况：

- 除了拥有Master权限的用户之外，它还阻止了每个人创建（如果尚未创建）
- 除了拥有Master权限的用户之外，它可以阻止每个人的推送
- 阻止任何人强行推到分支机构
- 阻止任何人删除分支

要保护一个分支，您需要至少拥**Master**权限级别。请注意，默认情况下，主分支是受保护的。

1. 导航到项目的主页面。
2. 在右上角，点击设置轮并选择**Protected branches**。
![Project settings list](./images/project_settings_list.png)

3. 从分支下拉菜单中，选择要保护的分支，然后单击保护。 在下面的截图中，我们选择了开发分支。
![Protected branches page](./images/protected_branches_page.png)

4. 一旦完成，受保护的分支将出现在**Protected branche**列表中。
![Protected branche list](./images/protected_branches_list.png)

从GitLab 8.11开始，增加了一个分支保护层，为保护分支提供了更精细的管理。 **Developers can push**选项被替换为**Allowed to push**设置，可以设置允许/禁止大师和/或开发人员推送到受保护的分支。

使用**Allowed to push**和**Allowed to merge**设置，您可以控制不同角色可以使用受保护的分支执行的操作。例如，您可以将**Allowed to push**设置为**No one**，并将**Allowed to merge**设置为**Developers + Masters**，要求每个人都提交合并请求以进入受保护的分支。这与GitLab工作流程等工作流程兼容。

然而，有一些工作流程不需要，只有防止强制推送和分支删除是有用的。对于这些工作流程，您可以通过将**Allowed to push**设置为**Developers + Masters**来允许具有写入权限的所有人推送到受保护的分支。

您可以在创建受保护的分支或之后通过从**protected branch**区域的下拉列表中选择所需的选项来设置**Allowed to push**和**Allowed to merge**选项。

![Protected branches devs can push](./images/protected_branches_devs_can_push.png)
